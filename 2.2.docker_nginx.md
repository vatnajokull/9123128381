# Nginx and Docker

В качестве веб сервиса будем использовать nginx.

Преждем чем перейти к созданию образа для сервиса nginx, создадим отдельную директорию в которой будем хранить все конфигурации внешних сервисов связанных с приложением.

```bash
cd ../
mkdir docker
cd docker
git init
```

Проинициализируем Dockerfile для nginx сервиса

```bash
mkdir nginx
touch nginx/Dockerfile
```

```Dockerfile
# Pull in the from the official nginx image.
FROM nginx:1.9

# We'll need curl within the nginx image.
RUN apt-get update \
      && apt-get install -y --no-install-recommends curl \
      && rm -rf /var/lib/apt/lists/*

# Delete the default welcome to nginx page.
RUN rm /usr/share/nginx/html/*

# Copy over the custom nginx and default configs.
COPY configs/nginx.conf /etc/nginx/nginx.conf
COPY configs/default.conf /etc/nginx/conf.d/default.conf

# Allow us to customize the entrypoint of the image.
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

# Start nginx in the foreground to play nicely with Docker.
CMD ["nginx", "-g", "daemon off;"]
```

Конфигурация nginx будет хранится в директории `configs`

```bash
mkdir nginx/configs
```

Для custom nginx конфигурации создадим `nginx.conf`

```bash
touch nginx/configs/nginx.conf
```

И запишим в `nginx.conf` следующие надстройки

```conf
# nginx/configs/nginx.conf

# Number of workers to run, usually equals number of CPU cores.
worker_processes auto;

# Maximum number of opened files per process.
worker_rlimit_nofile 4096;

events {
  # Maximum number of simultaneous connections that can be opened by a worker process.
  worker_connections 1024;
}

http {
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  # ---------------------------------------------------------------------------
  # Security settings from: https://gist.github.com/plentz/6737338

  # Disable nginx version from being displayed on errors.
  server_tokens off;

  # config to don't allow the browser to render the page inside an frame or iframe
  # and avoid clickjacking http://en.wikipedia.org/wiki/Clickjacking
  # if you need to allow [i]frames, you can use SAMEORIGIN or even set an uri with ALLOW-FROM uri
  # https://developer.mozilla.org/en-US/docs/HTTP/X-Frame-Options
  add_header X-Frame-Options SAMEORIGIN;

  # when serving user-supplied content, include a X-Content-Type-Options: nosniff header along with the Content-Type: header,
  # to disable content-type sniffing on some browsers.
  add_header X-Content-Type-Options nosniff;

  # This header enables the Cross-site scripting (XSS) filter built into most recent web browsers.
  # It's usually enabled by default anyway, so the role of this header is to re-enable the filter for
  # this particular website if it was disabled by the user.
  # https://www.owasp.org/index.php/List_of_useful_HTTP_headers
  add_header X-XSS-Protection "1; mode=block";
  # ---------------------------------------------------------------------------

  # Avoid situations where a hostname is too long when dealing with vhosts.
  server_names_hash_bucket_size 64;
  server_names_hash_max_size 512;

  # Performance optimizations.
  sendfile on;
  tcp_nopush on;

  # http://nginx.org/en/docs/hash.html
  types_hash_max_size 2048;

  # Enable gzip for everything but IE6.
  gzip on;
  gzip_disable "msie6";

  # Default config for the app backend.
  include /etc/nginx/conf.d/default.conf;
}
```

Для дефолтных конфигов создадим `default.conf`

```bash
touch nginx/configs/default.conf
```

И запишим в `default.conf` следующие надстройки.

```conf
upstream app {
  server APP_NAME:APP_PORT;
}

server {
  listen 80;
  server_name www.APP_VHOST;
  return 301 http://APP_VHOST$request_uri;
}

server {
  listen 80 default deferred;
  server_name APP_VHOST;

  access_log /var/log/nginx.access.log;
  error_log /var/log/nginx.error.log info;
  
  client_max_body_size 64M;
  keepalive_timeout 10;

  root /APP_NAME/public;

  index index.html;

  location / {
    try_files $uri @app_proxy;
  }

  location ~* \.(?:manifest|appcache|html?|json)$ {
    expires -1;
  }

  location ~* \.(?:css|js)$ {
    try_files $uri @app_proxy;
    expires 1y;
    access_log off;
    add_header Cache-Control public;
  }

  location ~* \.(?:jpg|jpeg|gif|png|ico|bmp|swf|txt|svg|ttf|woff)$ {
    try_files $uri @app_proxy;
    access_log off;
    expires max;
  }

  error_page 503 @maintenance;

  location @maintenance {
    rewrite ^(.*)$ /maintenance.html break;
  }

  # Load the web app back end with proper headers.
  location @app_proxy {
    proxy_redirect off;
    proxy_set_header Client-Ip $remote_addr;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    gzip_static on;
    proxy_pass http://app;
  }
}
```

Кастомизацию конфигов nginx в контексте контейнера определим в `docker-entrypoint.sh`

```bash
touch nginx/docker-entrypoint.sh
```

```sh
#!/usr/bin/env bash

# Exit the script as soon as something fails.
set -e

APP_NAME="server_app" # имя контейнера с запущенным spree приложением
APP_PORT="8000" # порт по которому доступно spree приложение
APP_VHOST=${CUSTOM_APP_VHOST:="$(curl http://169.254.169.254/latest/meta-data/public-hostname)"} # Хост виртуального сервера на AWS. It defaults to the AWS EC2 Public DNS address by pulling this info from EC2's metadata. This allows us to dynamically configure nginx at runtime. If custom variable defined then use it

DEFAULT_CONFIG_PATH="/etc/nginx/conf.d/default.conf"

# Replace all instances of the placeholders with the values above.
sed -i "s/APP_NAME/${APP_NAME}/g"    "${DEFAULT_CONFIG_PATH}"
sed -i "s/APP_PORT/${APP_PORT}/g"    "${DEFAULT_CONFIG_PATH}"
sed -i "s/APP_VHOST/${APP_VHOST}/g"  "${DEFAULT_CONFIG_PATH}"

# Execute the CMD from the Dockerfile and pass in all of its arguments.
exec "$@"
```

Так же добавим наш вебсервер в `docker-compose.yml` spree приложения

```yml
  web_server:
    build: ../docker/nginx
    environment:
      CUSTOM_APP_VHOST: localhost
    ports:
      - 80:80
    depends_on:
      - server_app
```

И запустим команду

```bash
docker-compose -p spreeproject up
```

После приложение будет доступно через nginx по `localhost`
