<!-- READ: https://habr.com/post/310460/ -->



# Dockerize spree application on AWS. Production Decision

1. What task it's solve?

2. Prepare Application
- Dockerize rails application
- Dockerize nginx
- Container security

3. Prepare AWS Services
- CLI
- Security Group
- ECS IAM Roles
- Cluster
- S3
- ECR
- Container's instances
- RDS
- ElasticCache
- LoadBalancer
- Upload to private ECR
- Task Definition
- Scheduler Task Definition

4. Continuous Deployment
- Deploy script
- Upgrade without downtime

5. AWS ECS cost

6. Deactivate all services
